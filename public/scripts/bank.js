let hasloan = false;
let outstandingLoan = 0;
let bankbalance = 0;
let paybalance = 0;
let selectedComputerID;

let response;
let computerJson;

function showOutstandingLoan(amount)
{
    let p = document.getElementById("outstandingloan");
    p.innerHTML = "Outstanding loan &nbsp;&nbsp;&nbsp; kr "
    p.innerHTML += amount;
    p.style.display="block";
}

function showBankBalance(){
    document.getElementById("balance").innerHTML = "Balance &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; kr" + bankbalance.toString();
}

function getALoan(){
    console.log("inside get a loan!")
    let amount = parseInt(document.getElementById("loanInputId").value);
    if (Number.isNaN(amount))
    {
        return
    }
    if (hasloan){
        document.getElementById("loanformdiv").innerHTML += " Already has a loan! ";
        return
    }
    if (amount > 2*bankbalance)
    {
        document.getElementById("loanformdiv").innerHTML += " Max size of loan superseeded! ";
        return
    }
    outstandingLoan = amount.toString();
    hasloan=true;
    ShowRepayLoanPay();
    showOutstandingLoan(amount);
    //i worry the "memory" inplications of this. can I get a stack overflow?
    /*
    var loanModal = new bootstrap.Modal(document.getElementById("getLoanModal"), {
        keyboard:false
    })
    loanModal.hide();
    */
}

function ShowRepayLoanPay(){
    document.getElementById("RepayLoanButton").style.display = "block";
}

function transferMoney(){
    if(hasloan)
    {
        let tenPercent = 0.1*paybalance;

        outstandingLoan -= tenPercent;
        paybalance -= tenPercent;
        showOutstandingLoan(outstandingLoan);
    }
    bankbalance += paybalance;
    paybalance = 0;
    showBankBalance();
    showPayBalance();
    return
    
}
function showPayBalance(){
    document.getElementById("payParagraph").innerHTML = "Pay &nbsp;&nbsp; kr " + paybalance.toString();
}

function work(){
    paybalance += 100;
    showPayBalance();
}


function repayLoan(){
    if(paybalance>=outstandingLoan)
    {
        let positive_rest = paybalance - outstandingLoan;
        bankbalance += positive_rest;
        outstandingLoan = 0;
        hasloan = false;
    }
    else{
        outstandingLoan -= paybalance;
    }
    paybalance = 0;

    showPayBalance();
    showBankBalance();
    showOutstandingLoan(outstandingLoan);
}

async function getComputers(){
    try {
        response = await fetch('https://noroff-komputer-store-api.herokuapp.com/computers');
        computerJson = await response.json(); //extract JSON from the http response
        console.log(computerJson);
        populateSelect(computerJson);
        selectedComputerID = computerJson[0].id;
        populateComputerLarge();
      }
    catch(error){
        console.error("MEH",error);
      }
}

function populateSelect(array){
    let select = document.getElementById("computers");
    for (let el of array)
    {
        let option = document.createElement("option");
        option.text = el.title;
        option.value = el.id;
        
        select.appendChild(option);
    }
    
}
function populateSelectFeature(text){
    let selectFeature = document.getElementById("laptop_features");
    selectFeature.innerHTML = text;
}

function populateComputerLarge(){
    let computer = computerJson[selectedComputerID-1];
    document.getElementById("laptop_large_title").innerHTML=computer.title;
    document.getElementById("laptop_large_subtitle").innerHTML = "Price: " + computer.price.toString();
    document.getElementById("laptop_large_text").innerHTML = computer.description;
    document.getElementById("laptop_large_picture").src = "https://noroff-komputer-store-api.herokuapp.com/" + computer.image;
}

function changeSelect(){
    let id = document.getElementById("computers").value;
    selectedComputerID = parseInt(id);
    populateComputerLarge();
}

function byLaptop(){
    let selectedComputerPrice = computerJson[selectedComputerID-1].price;
    if (selectedComputerPrice <= bankbalance){
        bankbalance -= selectedComputerPrice;
        showBankBalance();
        alert("You are now the proud owner of the computer " + computerJson[selectedComputerID-1].title)
    } 
    else{
        alert("You can not afford this computer!")

    }
}
showBankBalance();
getComputers();
